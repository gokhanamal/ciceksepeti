package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"

	"github.com/tealeg/xlsx"
)

type Kota struct {
	min, max int
}
type Bayi struct {
	x, y float64
	isim string
	kota Kota
}

type Order struct {
	Bayi      string  `json:"bayi"`
	SiparisNo string  `json:"siparisNo"`
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
}

func main() {
	bayiler := [3]Bayi{
		Bayi{41.049792, 29.003031, "Kirmizi", Kota{20, 30}},
		Bayi{41.069940, 29.019250, "Yesil", Kota{20, 30}},
		Bayi{41.049997, 29.026108, "Mavi", Kota{20, 30}},
	}

	readExcel(bayiler)
}

func distance(x1, y1, x2, y2 float64) float64 {
	return math.Sqrt(math.Pow(x2-x1, 2.0) + math.Pow(y2-y1, 2.0))
}

func readExcel(bayiler [3]Bayi) {
	orders := []Order{}
	redKota := 0
	blueKota := 0
	greenKota := 0
	excelFileName := "./excel.xlsx"
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		println(err)
	}
	for _, sheet := range xlFile.Sheets {
		if sheet.Name == "Siparişler" {
			for rowIndex, row := range sheet.Rows {
				if rowIndex > 0 {
					var y float64
					var x float64
					var bayi1 float64
					var bayi2 float64
					var bayi3 float64
					var siparisNo string
					for cellIndex, cell := range row.Cells {
						text := cell.String()

						if cellIndex == 1 {
							x, _ = strconv.ParseFloat(text, 64)
						}
						if cellIndex == 2 {
							y, _ = strconv.ParseFloat(text, 64)
						}
						if cellIndex == 0 {
							siparisNo = text
						}
						bayi1 = distance(bayiler[0].x, bayiler[0].y, x, y)
						bayi2 = distance(bayiler[1].x, bayiler[1].y, x, y)
						bayi3 = distance(bayiler[2].x, bayiler[2].y, x, y)
					}
					if bayi1 > bayi2 {
						if bayi1 > bayi3 && redKota <= 30 {
							var order = Order{
								Bayi:      "red",
								SiparisNo: siparisNo,
								Latitude:  x,
								Longitude: y,
							}
							orders = append(orders, order)
							redKota++
						} else if blueKota <= 80 {
							var order = Order{
								Bayi:      "blue",
								SiparisNo: siparisNo,
								Latitude:  x,
								Longitude: y,
							}
							orders = append(orders, order)
							blueKota++
						}
					} else {
						if bayi2 > bayi3 && greenKota <= 50 {
							var order = Order{
								Bayi:      "green",
								SiparisNo: siparisNo,
								Latitude:  x,
								Longitude: y,
							}
							orders = append(orders, order)
							greenKota++
						} else if blueKota <= 80 {
							var order = Order{
								Bayi:      "blue",
								SiparisNo: siparisNo,
								Latitude:  x,
								Longitude: y,
							}
							orders = append(orders, order)
							blueKota++
						}
					}
				}
			}
		}
	}
	pagesJson, err := json.Marshal(orders)
	if err != nil {
		log.Fatal("Cannot encode to JSON ", err)
	}
	fmt.Fprintf(os.Stdout, "%s", pagesJson)
}
