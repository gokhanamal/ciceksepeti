
async function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: { lat: 41.063848, lng: 29.0075001 },
    });

    const apiRequest = await fetch('api/index.json');
    const data = await apiRequest.json();
    data.forEach(item => {
        let contentString = `<div style="width:500px"><span style="display: block; font-size: 18px"><b>${item.siparisNo}</b></span></div>`

        let infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        let x = new google.maps.Marker({
            position: { lat: Number(item.latitude), lng: Number(item.longitude) },
            map: map,
            title: item.name,
            icon: {
                url: `http://maps.google.com/mapfiles/ms/icons/${item.bayi}-dot.png`
            }
        });

        x.addListener('mousedown', function () {
            infowindow.open(map, x);
        });
        x.addListener('mouseup', function () {
            setTimeout(() => {
                infowindow.close(map, x);
            }, 3000);
        });
    });
}



